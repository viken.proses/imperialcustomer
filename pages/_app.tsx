import "bootstrap/dist/css/bootstrap.css";
import "../styles/globals.scss";
import type { AppProps } from 'next/app'
import { UiProvider } from "App/contexts/ui/ui.provider";

function MyApp({ Component, pageProps }: AppProps) {
  return <UiProvider>
  <Component {...pageProps} />
  </UiProvider>
  
}

export default MyApp
