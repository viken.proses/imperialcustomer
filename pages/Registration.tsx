import { NextPage } from "next";
import React from "react";
import RegistrationModule from "App/modules/RegistrationModule/Registration";

const Registration: NextPage = () => {
  return <RegistrationModule />;
};

export default Registration;