import type { NextPage } from 'next'
import HomeModule from "../App/modules/HomeModule/Home"

const Home: NextPage = () => {
  return (
    <HomeModule />
  )
}

export default Home
