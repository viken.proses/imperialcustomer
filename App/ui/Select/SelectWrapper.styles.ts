import { cssFocusVisible, styled } from "../../theme/stitches.config";

export const StyledSelectWrapper= styled(
    'div', {
        textAlign: 'left',
        marginBottom: 15,
    },
    cssFocusVisible
  );
  