import React, { Fragment } from "react";
import Label from "App/ui/Label/Label";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import { SmallText } from "@ui/Input/Input.styles";
import { StyledSelectWrapper } from "./SelectWrapper.styles";

type propTypes = {
  onChange: (value: any, action: any) => void;
  items: any[];
  bindValue: string;
  bindName: string;
  disabled?: boolean;
  name?: string;
  placeholder?: string;
  isMulti?: boolean;
  value?: number | string | null;
  isClearable?: boolean;
  className?: string;
  label?: string;
  required?: boolean;
  error?: string | any;
};

function SelectMenu({
  onChange,
  items,
  bindValue,
  bindName,
  disabled,
  name,
  placeholder,
  isMulti,
  value,
  isClearable,
  className,
  label,
  required,
  error
}: propTypes) {
  const animatedComponents = makeAnimated();
  const customStyles: any = {
    control: () => ({
      alignItems: "center",
      backgroundColor: "#ffffff",
      border: "solid 2px #d90315",
      borderRadius: "7px",
      justifyContent: "space-between",
      maxHeight: "2.8rem",
      position: "relative",
      transition: "all 100ms",
      boxSizing: "border-box",
      outline: 0,
      display: "flex",
    }),
  };
  return (
    <Fragment>
      <StyledSelectWrapper>
      {label ? <Label css={{ mb: 5 }} label={label} required /> : null}

      {isMulti ? (
        <Select
          options={items}
          getOptionLabel={(option: any) => `${option[bindName]}`}
          getOptionValue={(option: any) => `${option[bindValue]}`}
          value={value}
          components={animatedComponents}
          isDisabled={disabled}
          isSearchable={true}
          placeholder={placeholder}
          onChange={onChange}
          closeMenuOnSelect={!isMulti}
          name={name}
          isMulti={isMulti}
          className={className}
          // menuPortalTarget={document.body}
          menuPosition={"fixed"}
          menuPlacement={"auto"}
        />
      ) : (
        <Select
          options={items}
          getOptionLabel={(option: any) => `${option[bindName]}`}
          getOptionValue={(option: any) => `${option[bindValue]}`}
          value={items?.filter((option: any) => {
            return option[bindValue] == value;
          })}
          instanceId="ID"
          components={animatedComponents}
          isDisabled={disabled}
          isSearchable={true}
          placeholder={placeholder}
          onChange={onChange}
          name={name}
          styles={customStyles}
          className={className}
          isClearable={isClearable || false}
          // menuPortalTarget={document.body}
          menuPosition={"fixed"}
          menuPlacement={"auto"}
        />
      )}
      {error ? <SmallText type="error">{error}</SmallText> : null}
      </StyledSelectWrapper>
    </Fragment>
  );
}

export default SelectMenu;
