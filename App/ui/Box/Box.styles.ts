import { cssFocusVisible, styled } from "../../theme/stitches.config";


export const StyledBox = styled(
    'div', {
      variants: {
        display: {
          block: {
            display: 'block',
          },
          flex: {
            display: 'flex',
            flexWrap: 'wrap',
            width: "100%",
            "&>*": {
              flex: '1 0 0%',
              width: "100%",
              maxWidth: "100%",
            },
          },
          table: {
            display: 'table',
          },
          inlineBlock: {
            display: 'inline-block'
          },
        },
        align: {
          right: {
            justifyContent: 'right'
          },
          center: {
            justifyContent: 'center'
          },
        },
        alignSelt: {
          middle: {
            alignSelf: 'center'
          },
          bottom: {
            alignSelf: 'end'
          },
        },
        position: {
          middle: {
            alignItems: 'center',
          },
          bottom: {
            alignItems: 'end',
          },
        },
        height: {
          full: {
            height: '100vh'
          },
        }


      },
      defaultVariants: {
      },
      '& .techSpecs': {
        borderTop: '1px solid #B9BCBF',
        borderBottom: '1px solid #B9BCBF',
        p: '$15'
      },
      '& .customerSupport': {
        px: '$10',
        py: '$15'
      },
      '& .addressList': {
        px: '$10',
        py: '$5',
        mb: '$10',
        border: '1px solid #eee',
        borderRadius: 10,
        cursor: 'pointer'
      }
    },
    cssFocusVisible
  );
  