import React from 'react'
import {PopoverArrow, PopoverContent } from './Popover.styles';

export const PopoverComp = React.forwardRef(
    ({ children, ...props }: any, forwardedRef) => (
      <PopoverContent sideOffset={5} {...props} ref={forwardedRef}>
        {children}
        <PopoverArrow />
      </PopoverContent>
    )
  );