import React from "react";
import Box from "../Box/Box";
import Label from "../Label/Label";
import { CheckboxVariantsProps, StyledCheckbox } from "./Checkbox.styles";

interface Props {
  checked?: boolean;
  value?: string | number;
  disabled?: boolean;
  preventDefault?: boolean;
  size?: string;
  color?: string;
  textColor?: any;
  id:any
}

type LabelProps = {
  label: string;
};

type NativeAttrs = Omit<React.InputHTMLAttributes<unknown>, keyof Props>;
type CheckboxProps = NativeAttrs & CheckboxVariantsProps & LabelProps & Props;

const Checkbox = ({ label, ...rest }: CheckboxProps) => {
  return (
    <Box>
      <StyledCheckbox type="checkbox" {...rest} />
      {label ? (
        <Label css={{pl: 15}} label={label} htmlFor={rest.id} required={rest.required} />
      ) : null}
    </Box>
  );
};

export default Checkbox;
