import { cssFocusVisible, styled, VariantProps } from "../../theme/stitches.config";

export const StyledCheckbox = styled(
  'input', {
    // my: 0,
    position: 'relative',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '$borderLight',
    verticalAlign: 'middle',
    "&:focus-visible" : {
      outline: "none",
    },
    "&:before" : {
        content: '',
        position: 'absolute',
        display: 'block',
        borderRadius: 2,
        borderWidth: 2,
        borderStyle: 'solid',
        height: '100%',
          width: '100%',
          borderColor: '$indigo9',
          backgroundColor: 'white'
    },
    "&:checked:after" : {
      content: '',
      position: 'absolute',
      display: 'block',
      borderRadius: '2',
      borderWidth: 2,
      borderStyle: 'solid',
      borderTopColor: 'transparent',
      borderRightColor: 'transparent',
      borderLeftColor: '$primary',
      borderBottomColor: '$primary',
      transform: 'rotate(-45deg) translate(-50%, -50%)',
      height: '40%',
      width: '65%',
      left: '52%',
      top: '5%'
      // marginLeft: 4,
      // marginTop: 3,
      // backgroundColor: '$indigo9',
    },
    variants: {
      size: {
        xs: {
          height: '10',
          width: '10',
        },
        sm: {
          height: 15,
          width: 15,
        },
        md: {
          height: 20,
          width: 20,
        },
        lg: {
          height: 25,
          width: 25,
        },
        xl: {
          height: 30,
          width: 30,
        }
      },
      color: { 
        default: {
          backgroundColor: "transparent",
          color: '$indigo4',
          "&:before" : {
                borderColor: '$primary',
                backgroundColor: 'white'
          },
          "&:checked:after" : {
            // backgroundColor: '$indigo9',
          },
        }
      },
      error: {
        true: {
          borderColor: 'red',
        }
      },
      success: {
        true: {
          borderColor: 'green',
        }
      },
    },
    defaultVariants: {
      color: 'default',
      // weight: 'normal',
      // style: 'normal',
      size: 'sm',
    }
  },
    cssFocusVisible
  );
  

export type CheckboxVariantsProps = VariantProps<typeof StyledCheckbox>;
