import { ACTION_TYPE, GRID_STATE_TYPE, LS_KEYS } from "./type";
import { formatHeaderData, getLS, sanitizeFilters, setLS } from "./utils";

export let initialState: GRID_STATE_TYPE = {
  gridOutsideConfig: {
    page: 1,
    limit: 10,
    filters: {},
  },
  densitySelected: "standard",
  columns: [],
  emitFilterValues: false,
  hideFilter: false,
  currentStyle: "bordered",
};

export const gridReducer = (
  state = initialState,
  action: { type: any; payload: any }
): GRID_STATE_TYPE => {
  let { type, payload } = action;
  switch (type) {
    case ACTION_TYPE.HYDRATE_STATE:
      let cs = getLS(LS_KEYS.GRID_STYLE);
      let ds = getLS(LS_KEYS.GRID_DENSITY);
      if (!cs) {
        setLS(LS_KEYS.GRID_STYLE, "bordered");
      }
      if (!ds) {
        setLS(LS_KEYS.GRID_DENSITY, "standard");
      }
      return { ...state, currentStyle: cs, densitySelected: ds };

    case ACTION_TYPE.STYLE_CHANGE:
      setLS(LS_KEYS.GRID_STYLE, payload);
      return { ...state, currentStyle: payload };

    case ACTION_TYPE.HIDE_FILTER_CHANGE:
      return { ...state, hideFilter: !state.hideFilter };

    case ACTION_TYPE.COLUMN_CHANGE:
      return { ...state, columns: [...payload] };

    case ACTION_TYPE.SET_INITIAL_COLUMN:
      let arr = formatHeaderData(payload);
      return { ...state, columns: [...arr] };

    case ACTION_TYPE.SET_ITEM_PER_PAGE:
      return {
        ...state,
        emitFilterValues: true,
        gridOutsideConfig: { ...state.gridOutsideConfig, limit: payload },
      };

    case ACTION_TYPE.DENSITY_CHANGE:
      setLS(LS_KEYS.GRID_DENSITY, payload);
      return { ...state, densitySelected: payload };

    case ACTION_TYPE.FILTER_CHANGE:
      let sanitizedFilters = sanitizeFilters({
        ...state.gridOutsideConfig.filters,
        ...payload,
      });
      return {
        ...state,
        emitFilterValues: true,
        gridOutsideConfig: {
          ...state.gridOutsideConfig,
          filters: sanitizedFilters ? { ...sanitizedFilters } : null,
        },
      };

    case ACTION_TYPE.PAGE_CHANGE:
      return {
        ...state,
        emitFilterValues: true,
        gridOutsideConfig: {
          ...state.gridOutsideConfig,
          page: payload,
        },
      };
    default:
      return state;
  }
};
