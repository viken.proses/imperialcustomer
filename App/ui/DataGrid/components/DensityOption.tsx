import Text from "@ui/Text/Text";
import SettingIcon from "App/icons/SettingIcon";
import React from "react";
import { TriggerButton } from "../DataGrid.styles";
import { ArrowLeftIcon, ArrowRightIcon } from "../icons";
import { densityDropdown, styleOptions } from "../staticData";
import { DENSITY_TYPE } from "../type";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
  DropdownMenuTriggerItem,
  RightSlot,
} from "./column.styles";

function DensityOption({
  setdensitySelected,
  densitySelected,
  hideFilterChange,
  hideFilter,
  handleStyleChange,
  currentStyle
}: {
  setdensitySelected: any;
  densitySelected: DENSITY_TYPE;
  hideFilterChange: any;
  hideFilter: boolean;
  handleStyleChange:any,
  currentStyle:any
}) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <TriggerButton>
          <SettingIcon />
          <Text css={{ writingMode: "vertical-lr", lineHeight: '6px', margin: '10px 5px'}}>Settings</Text>
        </TriggerButton>
      </DropdownMenuTrigger>
      <DropdownMenuContent sideOffset={5} side="left">
        <DropdownMenuItem focusEffect onSelect={hideFilterChange}>
          {hideFilter ? "Show" : "Hide"} filters
        </DropdownMenuItem>
        <DropdownMenu>
          <DropdownMenuTriggerItem>
            Density{" "}
            <RightSlot>
              <ArrowRightIcon />
            </RightSlot>
          </DropdownMenuTriggerItem>
          <DropdownMenuContent align="end">
            {densityDropdown.map((item) => {
              return (
                <DropdownMenuItem
                  activeDensity={item.name == densitySelected}
                  onSelect={() => setdensitySelected(item.name)}
                  key={item.id}
                  focusEffect
                >
                  {item.name} <RightSlot>{item.icon}</RightSlot>
                </DropdownMenuItem>
              );
            })}
          </DropdownMenuContent>
        </DropdownMenu>
        <DropdownMenu>
          <DropdownMenuTriggerItem>
            Style
            <RightSlot>
              <ArrowRightIcon />
            </RightSlot>
          </DropdownMenuTriggerItem>
          <DropdownMenuContent align="end">
            {styleOptions.map((item) => {
              return (
                <DropdownMenuItem
                  activeDensity={item.name == currentStyle}
                  onSelect={() => handleStyleChange(item.name)}
                  key={item.name}
                  focusEffect
                >
                  {item.name}
                </DropdownMenuItem>
              );
            })}
          </DropdownMenuContent>
        </DropdownMenu>
      </DropdownMenuContent>
    </DropdownMenu>
    // <DropdownStyled
    //   arr={densityDropdown}
    //   align="end"
    //   trigger={
    //     <TriggerButton aria-label="Customise options">
    //       <Text>{densitySelected}</Text>
    //     </TriggerButton>
    //   }
    //   triggerProps={{ asChild: true }}
    //   onSelect={(val: any) => setdensitySelected(val.name)}
    // />
  );
}

export default React.memo(DensityOption);
