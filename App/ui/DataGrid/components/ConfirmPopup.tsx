import { Button } from '@ui/Button/Button'
import Group from '@ui/Group/Group'
import Spacer from '@ui/Spacer/Spacer'
import Text from '@ui/Text/Text'
import React, { useState } from 'react'
import Popover from '../../Popover'
import { actionButtonType } from '../type'

function ConfirmPopup({item, onConfirm}:{item: actionButtonType, onConfirm: () => void}) {
  return (
    <Popover defaultOpen={false}>
    <Popover.Trigger asChild>
      <span>{item.icon}</span>
    </Popover.Trigger>
    <Popover.Content side="top">
     <Text>{item.confirmBox?.title}</Text>
     <Spacer y={5} />
      <Group>
      <Popover.Close asChild>
        <Button bordered size="xs" onClick={onConfirm}><Text>{item.confirmBox?.confirmText}</Text></Button>
        </Popover.Close>
        <Popover.Close asChild>
        <Button bordered size="xs" color="error"><Text>{item.confirmBox?.cancelText}</Text></Button>
        </Popover.Close>
      </Group>
    </Popover.Content>
  </Popover>
  )
}

export default ConfirmPopup