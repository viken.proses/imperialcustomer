import Switch from "../../Switch/Switch";
import React from "react";
import { TriggerButton } from "../DataGrid.styles";
import { ColumnTypes } from "../type";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "./column.styles";
import Text from "@ui/Text/Text";
import ColumnIcon from "App/icons/ColumnIcon";

function ColumnOptions({
  columns,
  handleColumnChange,
}: {
  columns: ColumnTypes[];
  handleColumnChange: (visibility: boolean, index: number) => void;
}) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <TriggerButton>
          <ColumnIcon />
          <Text css={{ writingMode: "vertical-lr", lineHeight: '6px', margin: '10px 5px'}}>Columns</Text>
        </TriggerButton>
      </DropdownMenuTrigger>
      <DropdownMenuContent sideOffset={5} side="left">
        {columns.map((item, index) => {
          return (
            <DropdownMenuItem
              onSelect={(e) => e.preventDefault()}
              key={item.title}
            >
              <Switch
                label={item.title}
                size="sm"
                checked={item.visible}
                onChange={(e) => handleColumnChange(e, index)}
              />
            </DropdownMenuItem>
          );
        })}
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export default React.memo(ColumnOptions) ;
