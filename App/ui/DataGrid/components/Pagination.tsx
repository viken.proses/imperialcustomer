import React from "react";
import { StyledPagination } from "../DataGrid.styles";
import { ArrowLeftIcon, ArrowRightIcon } from "../icons";

type props = {
  totalCount: number;
  limit: number;
  onPageChange: (page: any) => void;
};

function Pagination({ totalCount, limit, onPageChange }: props) {
  return (
    <StyledPagination
      previousLabel={<ArrowLeftIcon />}
      nextLabel={<ArrowRightIcon />}
      breakLabel={"..."}
      breakClassName={"break-me"}
      pageCount={Math.ceil(totalCount / limit)}
      marginPagesDisplayed={5}
      pageRangeDisplayed={3}
      onPageChange={onPageChange}
      activeClassName={"activeClass"}
      className="active"
    />
  );
}

export default Pagination;
