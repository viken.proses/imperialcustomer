import Group from '@ui/Group/Group'
import React from 'react'
import { SingleItemType } from './Dropdown'
import { DropdownMenuItem, RightSlot } from './Dropdown.styles'

type SingleItemProps = {
    item: SingleItemType,
    onSelect: (item: SingleItemType, index: number) => void
    index: number,
    className?: string
}



function SingleItem({item, onSelect, index, className}: SingleItemProps) {
  return (
    <DropdownMenuItem
    key={item.name}
    onClick={() => onSelect(item, index)}
    className={className}
  >
    {item.name} {item.icon ? <RightSlot>{item.icon}</RightSlot> : null}
  </DropdownMenuItem>
  )
}

export default SingleItem