import { cssFocusVisible, styled } from "../../theme/stitches.config";


export const StyledTextareaWrapper= styled(
    'div', {
        textAlign: 'left',
        marginBottom: 15,
        error: {
            true: {
              borderColor: "$eborder",
              '&:focus': {
                outlineColor: "$red8",
              },
            }
          },
    },
    
    cssFocusVisible
  );
  
  export const SmallText = styled('div', {
    variants : {
      type: {
        error:  {
          color: "$ebgSolid1",
          fontSize: "$xs"
        },
        note: {
          color: "$gray9",
          fontSize: "12px"
        }
      }
    }
   })