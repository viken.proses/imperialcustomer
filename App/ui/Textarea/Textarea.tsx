import React from 'react'
import { StyledTextarea } from './Textarea.styles'
import { SmallText, StyledTextareaWrapper } from './TextareaWrapper.styles';
import Label from 'App/ui/Label/Label';
import Box from '@ui/Box/Box';

function Textarea(props: any) {
  const {label, children, error, note, ...rest} = props;
  return (
    <StyledTextareaWrapper>
    {label ?  <Label css={{mb: 5}}   htmlFor={props.id} label={label} required={props.required} /> : null }
    <StyledTextarea {...rest} /> 
    <Box>
    {note && !error ? <SmallText type="note">{note}</SmallText> : null } 
    {error ? <SmallText type="error">{error}</SmallText> : null}
    </Box>
    </StyledTextareaWrapper>
  )
}

export default Textarea