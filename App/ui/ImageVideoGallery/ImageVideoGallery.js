import React, { useState, useEffect } from "react";
import ImageGallery from "react-image-gallery";
import ImageZoom from "../ImageZoom/ImageZoom";

// const ImageVideoGallery = ({ sourceArr, diamond }) => {
const ImageVideoGallery = ({ sourceArr }) => {

  // console.log(sourceArr, "sourceArr")
  const [finalSource, setfinalSource] = useState([]);
  const { productImages, productVideo } = sourceArr;
  // console.log(productImages, "this is product images");
  // console.log(productVideo, "this is product videos");
  if (productVideo?.length) {
    productVideo.map((vdo) => {
      Object.assign(vdo, {
        renderItem: (vdo) => _renderVideo(vdo),
      });
    });
  }

  useEffect(() => {
    let final = [];
    if (productImages?.length) {
      let tempProductImages = productVideo ? productImages.concat(productVideo) : productImages;

      final = tempProductImages.map((item) => {
        if (!item?.check && !item?.Video) {
          // item['thumbnail'] = `${item?.thumbnail}`
          // item['original'] = `${item?.original}`,

          item["thumbnail"] = `${item?.thumbnail}`;
          (item["original"] = `${item?.original}`), (item["check"] = true);
        } else if (item?.Video) {
          item["thumbnail"] = "/play-button.png";
          item["check"] = true;
        }
        // console.log(tempProductImages,"tempProductImages");
        return item;
      });
    }

    if (!final.length)
      final.push({
        thumbnail: "/img/noimage.jpg",
        original: "/img/noimage.jpg",
      });

    setfinalSource(final);

  }, [productImages, productVideo]);

  function _renderVideo(item) {

    return (
      <div>
        <div className="video-wrapper">

          {
            item ? (
              <video width="100%" height="200" controls autoPlay={true} loop muted>
                <source
                  src={item.original}
                  type="video/mp4"
                />
              </video>
            ) : null
          }

          {/* <iframe
            width="100%"
            height="100%"
            // src={`${item?.original}`}
            src={item.original}
            frameBorder="0"
            allowfullscreen
            sandbox="allow-scripts allow-presentation allow-same-origin"
            allow="autoplay; fullscreen; picture-in-picture; xr-spatial-tracking; clipboard-write"
          ></iframe> */}

        </div>
      </div>
    );
  }

  //properties for image gallery
  const properties = {
    showThumbnails: "true",
    infinite: "true",
    thumbnailPosition: "bottom",
    showNav: true,
    items: finalSource,
    showPlayButton: false,
    showFullscreenButton: false,
    disableThumbnailScroll: false,

    renderItem: (item) => (
      <>
        <ImageZoom item={item} />
      </>
    ),
  };

  return (
    <section className="image_video_gallery">
      <ImageGallery {...properties} />
    </section>
  );
};

export default ImageVideoGallery;
