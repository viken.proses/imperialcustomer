// import { addUser } from 'App/api/user';
import Box from "@ui/Box/Box";
// import Card from "@ui/Card";
import Layout, { toastAlert } from "App/shared/components/Layout";
import React, { useContext, useEffect, useReducer, useState } from "react";
// import Banner from "./components/Banner";
import ImagePreview from "./component/ImagePreview";
import Detail from "./component/Detail";
import Info from "./component/Info";
import { useRouter } from "next/router";

// import { initialState, ProductReducer } from "./productDetailsReducer";
// import { getProductDetails } from "App/api/productlist";
// import { ITEM_ACTION_TYPES } from "./productDetailsTypes";
// import { addCart, getCart } from "App/api/CartAPI";
import { NODE_API_URL, PRODUCT_TYPE } from "App/utils/constants";
import CartCountContext from "App/contexts/cartCount/cartCount.context";
import { handleServerError } from "App/utils/helpers";
import Text from "@ui/Text/Text";
// import { NextSeo } from "next-seo";
import styles from "./ProductDetail.module.scss"

function ProductDetail() {
     
    let productImages:any=[{
        ID: 1,
        thumbnail: `/TAJIMAChalkBox.png`,
        original: `/TAJIMAChalkBox.png`,
        check: false,
        Video: false,
      },
      {
        ID: 2,
        thumbnail: `/TAJIMAChalkBox.png`,
        original: `/TAJIMAChalkBox.png`,
        check: false,
        Video: false,
      }]
    let productVideo:any=[
        {
            ID: 2,
            thumbnail: ``,
            original: ``,
            check: false,
            Video: true,
          }
    ]
  return (
    <Layout>
      {/* {
        state.productList ? (
          <NextSeo
            title={state?.productList?.itemName}
            description={state?.productList?.shortDescription}
          />
        ) : null
      } */}

      {/* <NextSeo
        title={`${state?.productList?.itemName}`}
        description={`${state?.productList?.shortDescription}`}
      /> */}

      {/* <NextSeo
        title="Simple Usage Example"
        description="A short description goes here."
      /> */}

      <Box className="container pt-5">
        <Box className="row">
          <Box className="col col-12 col-md-6 mb-5">
            <Text size="h3" weight="bold" className="pb-4">TAJIMA Chalk Box - Chalk-Rite ll</Text>
            <ImagePreview sourceArr={{ productImages, productVideo }} />
          </Box>
          <Box className="col col-12 col-md-6 mb-5">
            <Detail />
          </Box>
          <Box className="col col-12 mt-5">
            <Info/>
          </Box> 
        </Box>
        {/* <Card>
          <Banner />
        </Card> */}
      </Box>
    </Layout>
  );
}

export default ProductDetail;
