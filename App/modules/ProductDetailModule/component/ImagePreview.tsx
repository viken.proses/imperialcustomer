import Box from '@ui/Box/Box'
import Card from '@ui/Card'
import Text from '@ui/Text/Text'
import ImageVideoGallery from '@ui/ImageVideoGallery/ImageVideoGallery'
import Image from 'next/image'
import React from 'react'
import styles from "../ProductDetail.module.scss";

function ImagePreview({sourceArr}:any) {

    return (
        <>
            <Box className={`${styles.imageVideoGallery}`}>
                {/* <img className='img-fluid' src={`/cctv.png`} alt="cctv" /> */}
                <ImageVideoGallery
                    // sourceArr={{ productImages, productVideo }}
                    // diamond={diamond}

                    sourceArr={{ productImages: sourceArr?.productImages, productVideo: sourceArr?.productVideo }}
                />
            </Box>
            {/* <Text size="h2" font="bold" className='mb-2'>ZANO CCTV Pro</Text>
            <Text size="h3">Zano CCTV Pro Camera 1080p</Text> */}
        </>
    )
}

export default ImagePreview