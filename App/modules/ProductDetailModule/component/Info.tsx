import Box from '@ui/Box/Box'
import Card from '@ui/Card'
import { Column, ColumnParent, ColumnRow, DataCell, DataParent, DataRow, Table } from '@ui/DataGrid/DataGrid.styles'
import { StyledTabs, TabsContent, TabsList, TabsTrigger } from '@ui/Tabs/Tabs.styles'
import Text from '@ui/Text/Text'
import RightArrow from 'App/icons/RightArrow'
import Support from 'App/icons/Support'
import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

function Info() {
  return (
    <>
      <StyledTabs defaultValue="tab1">
        <TabsList aria-label="Manage your account">
          <TabsTrigger value="tab1">About this item</TabsTrigger>
          <TabsTrigger value="tab2">Technical Specification</TabsTrigger>
        </TabsList>
        <TabsContent value="tab1">
          <div className='textInner'>about test item</div>
        </TabsContent>
        <TabsContent value="tab2">
          test 2
        </TabsContent>
      </StyledTabs>
    </>
  )
}

export default Info