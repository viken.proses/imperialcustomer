import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import QTYMinus from "App/icons/QTYMinus";
import QTYPlus from "App/icons/QTYPlus";
import ConfirmBox from "App/shared/components/ConfirmBox/ConfirmBox";
import React from "react";
import Popover from "@ui/Popover";
import Group from "@ui/Group/Group";
import Spacer from "@ui/Spacer/Spacer";
import MinusCircle from "App/icons/MinusCircle";
import PlusCircle from "App/icons/PlusCircle";
import styles from "../ProductDetail.module.scss";
import Facebook from "App/icons/Facebook";
import Instagram from "App/icons/Instagram";
import Twitter from "App/icons/Twitter";
import Pinterest from "App/icons/Pinterest";

function Detail() {
  return (
    <>
      <Box>
        <Box className="pb-2">
          <Text color="primary" size="h6" className="text-end" weight="bold">
            Home/Hanging Tools
          </Text>
        </Box>
        <Text
          size="h5"
          weight="bold"
          className="text-center text-md-start mb-3 mt-3"
        >
          Product Discription
        </Text>
        <Text size="h6" weight="bold">
          The Tajima Chalk Box is a uniquely designed and made product that has
          been passed down through generations. This sleek, modern-looking chalk
          box has a specially designed compartment that is divided into three
          separate sections: the front section, the middle section, and the back
          section. It also features a unique silver clip that is easy to use and
          to attach to a belt or bag. The clip makes it easy to take this
          product anywhere, and the compact size makes it easy to fit in purses,
          backpacks, or briefcases.
        </Text>
        <hr />
        <Text
          size="h3"
          weight="bold"
          className="text-start mb-3"
        >
          $ 550.00
        </Text>
        <Box className="my-2">
          <Text size="h5" weight="bold">color : Red</Text>
        </Box>
        <Box className="my-2">
          <Text size="h5" weight="bold">Size: S</Text>
        </Box>
        <Box className="mb-3 d-flex">
          <Box className={`d-flex ${styles.productSize}`}>
            <Text size="h6" className="px-2 py-1 mr-3 size">
              M
            </Text>
            <Text size="h6" className="px-2 py-1 mx-1 size">
              L
            </Text>
            <Text size="h6" className="px-2 py-1 mx-1 size">
              S
            </Text>
            <Text size="h6" className="px-2 py-1 mx-1 size">
              XL
            </Text>
            <Text size="h6" className="px-2 py-1 mx-1 size">
              XXL
            </Text>
          </Box>
          <Box className="d-flex align-items-center ms-4">
            <Text size="h6">Find my size</Text>
          </Box>
        </Box>
        <Box className="d-flex">
          <Box className={`d-flex align-items-center productPlusMinusIcons`}>
            <MinusCircle size="25" />
            <Text className="px-2">1</Text>
            <PlusCircle size="25" />
          </Box>
          <Box className="ms-5">
            <Button rounded>Add to Cart</Button>
          </Box>
        </Box>
        <hr />
        <Text size="h6" className="my-2">
          <span>Availability</span> : <span> In Stock</span>
        </Text>
        <Text size="h6" className="my-2">
          <span>SKU</span> : <span>14235</span>
        </Text>
        <Text size="h6" className="my-2">
          <span>categories</span>: <span>Hanging Tools</span>
        </Text>
        <Text className="my-2">Fresh shipping & return for orders over $200</Text>
        <Text size="h6" className="my-2">
          <span>Share on</span>:
          <span className="productSocialIcons">
            <span className="m-1"><Facebook /></span>
            <span className="m-1"><Instagram /></span>
            <span className="m-1"><Twitter /></span>
            <span className="m-1"><Pinterest /></span>
          </span>
        </Text>
        <hr />
      </Box>
    </>
  );
}

export default Detail;
