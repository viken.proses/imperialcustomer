import Box from "@ui/Box/Box";
import Checkbox from "@ui/Checkbox/Checkbox";
import Input from "@ui/Input";
import Text from "@ui/Text/Text";
import styles from "../ProductListing.module.scss";

export default function SideBar() {
  let filter = [
    {
      title: "catogries",
      values: [
        { menu: "Regular(White)" },
        { menu: "Mold Resistant(Green)" },
        { menu: "Fire code,Fire Rated(Red)" },
        { menu: "Grassmatt(Fiberglass)" },
        { menu: "sound proof" },
        { menu: "Cement(Baker) Board" },
      ],
      checked: true,
    },
    {
      title: "color",
      values: [
        { menu: "Black(100)" },
        { menu: "Blue(100)" },
        { menu: "Green(100)" },
        { menu: "Turquoise(100)" },
        { menu: "Brown(100)" },
      ],
      checked: false,
    },
    {
      title: "color",
      values: [
        { menu: "Black(100)" },
        { menu: "Blue(100)" },
        { menu: "Green(100)" },
        { menu: "Turquoise(100)" },
        { menu: "Brown(100)" },
        { menu: "Regular(White)" },
        { menu: "Mold Resistant(Green)" },
        { menu: "Fire code,Fire Rated(Red)" },
        { menu: "Grassmatt(Fiberglass)" },
        { menu: "sound proof" },
        { menu: "Cement(Baker) Board" },
      ],
      checked: false,
    },
  ];
  const List = (list: any) => {
    return (
      <Box className={styles.SideListMenuList}>
        {console.log(list, "my list`")}
        {list.list.map((listmenu: any) => {
          return <Text className="py-1">{listmenu.menu}</Text>;
        })}
      </Box>
    );
  };
  const CheckedList = (list: any) => {
    return (
      <Box className={styles.SideListMenuList}>
        {list.list.map((listmenu: any) => {
          return (
            <Box className="d-flex">
              <Checkbox label={listmenu.menu} id="" />
              {/* <Text className="py-1 ml-1">{listmenu.menu}</Text> */}
            </Box>
          );
        })}
      </Box>
    );
  };
  return (
    <>
      <Box className="Sidebar-menu">
        {filter.map((filterItem) => {
          return (
            <Box className="mb-2">
              <Box className={`${styles.sideMenuHradingBg} p-2`}>
                <Text font="bold" size="h3" transform="capitalize">
                  {filterItem.title}
                </Text>
              </Box>
              <Box className={`${styles.sideMenuList} p-2`}>
                {console.log(filterItem.values, "filterdde itemss")}
                <>
                  {filterItem.values.length > 5 ? <Input /> : null}
                  {filterItem.checked ? (
                    <CheckedList list={filterItem?.values} />
                  ) : (
                    <CheckedList list={filterItem?.values} />
                  )}
                </>
                {/* ); */}
                {/* })} */}
              </Box>
            </Box>
          );
        })}
      </Box>
    </>
  );
}
