import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Card from "@ui/Card";
import { TriggerButton } from "@ui/DataGrid/DataGrid.styles";
import DropdownStyled from "@ui/Dropdown/Dropdown";
import Text from "@ui/Text/Text";
import DownArrow from "App/icons/DownArrow";
import MinusCircle from "App/icons/MinusCircle";
import PlusCircle from "App/icons/PlusCircle";
import UserIcon from "App/icons/UserIcon";
import { DropdownGroup } from "App/shared/components/Header/Header.styles";
import SingleProductCard from "App/shared/components/ProductCard/SingleProductCard";
import React from "react";
import { ProductCard } from "../ProductLising.styles";
import styles from "../ProductListing.module.scss";

function Product() {
  let productArray = [
    {
      img: "./series.png",
      price: "$35.38",
      productTitle: "GRATE-LITE\u2122 F Series",
      discription: "3 Lightning modes of brightness: 500lm, 250lm, 50lm",
    },
    {
      img: "./series.png",
      price: "$35.38",
      productTitle: "GRATE-LITE\u2122 F Series",
      discription: "3 Lightning modes of brightness: 500lm, 250lm, 50lm",
    },
    {
      img: "./series.png",
      price: "$35.38",
      productTitle: "GRATE-LITE \u2122 F Series",
      discription: "3 Lightning modes of brightness: 500lm, 250lm, 50lm",
    },
  ];

  const ACTIONS = [
    { name: "My Profile", id: 1 },
    { name: "My Orders", id: 4 },
    { name: "Change Password" },
    { name: "Logout", id: 3 },
  ];

  return (
    <Box className="container">
      <Box className="py-2 justify-content-between justify-content-md-end  d-flex">
        <Button className="d-md-none d-sm-block">Filter</Button>
        <Box className="d-flex align-items-center me-0">
          <DropdownGroup>
            <DropdownStyled
              className="headerDropdown"
              arr={ACTIONS}
              trigger={
                <TriggerButton
                  css={{ position: "relative" }}
                  aria-label="Customise options"
                  className={styles.productShortButton}
                >
                  short by : default <DownArrow />
                </TriggerButton>
              }
              triggerProps={{ asChild: true }}
              onSelect={(e: any) => /* handleChange(e.id)*/ e}
            />
          </DropdownGroup>
        </Box>
      </Box>
      <Box className="row row-cols-md-3 row-cols-1">
        {productArray.map((product) => {
          return (
            <Box className="col py-3 py-md-0">
              <SingleProductCard product={product}/>
            </Box>
          );
        })}
      </Box>
    </Box>
  );
}

export default Product;
