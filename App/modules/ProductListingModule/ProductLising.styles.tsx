import { styled } from "App/theme/stitches.config";
import Card from "@ui/Card";


export const ProductCard = styled(Card, {
    width: '100%',
    minHeight: 300,
    padding:0,
    // marginTop: '$15',
    // // marginBottom: '$15',
    marginLeft: "auto",
    marginRight: "auto",
    ".img-fluid": {
      height: 250,
      objectFit: 'contain'
    },
  })