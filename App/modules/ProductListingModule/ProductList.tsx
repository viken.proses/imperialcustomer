import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Layout from "App/shared/components/Layout";
import Product from "./components/Product";
import SideBar from "./components/SiderBar";
import styles from "./ProductListing.module.scss"

export default function Productlist(){
    return(
        <>
        <Layout>
            <Box className="container py-4">
                <Box className="row pb-2 head">
                    <Text color="dark" font="bold" size="h1" >DryWall Boards</Text>
                    <Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, quod necessitatibus hic beatae molestias iste deleniti magni esse, adipisci culpa nesciunt asperiores officia voluptates ullam labore qui minus nihil? Aliquid!</Text>
                </Box>
                <Box className="row ">
                    <Box className="col clo-md-3 d-none d-sm-none d-md-block">
                        <SideBar/>
                    </Box>
                    <Box className={`col col-md-9 ${styles.ProductCol}`}>
                        <Product/>
                    </Box>
                </Box>
            </Box>
        </Layout>
        </>
    )
}