import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Image from "next/image";
import "keen-slider/keen-slider.min.css"
import { useKeenSlider } from "keen-slider/react"
import HomeCommonSlider from "./homeCommonSlider";

export default function Topseller() {
  return (
    <>
      <Box>
        <Box className="containetr">
          <Box className="d-flex justify-content-center ">
            <Box className="text-center">
              <Image src={`/crown.png`} width={100} height={46} />
              <Text size="h3" weight="bold">
                TOP SELLERS
              </Text>
            </Box>
          </Box>
          <Box className="row">

          <HomeCommonSlider/>

          </Box>
        </Box>
      </Box>
    </>
  );
}
