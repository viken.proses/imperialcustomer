import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import Image from "next/image";

export default function Department() {
  let departmentArray = [
    { imageUrl: "DrywallBoards.png", imageText: "Drywall Boards" },
    { imageUrl: "AutomaticTapingTool.png", imageText: "Automatic Taping Tool" },
    { imageUrl: "AutomaticTapingParts.png", imageText: "Automatic Taping Parts" },
    { imageUrl: "FinishingTools.png", imageText: "Finishing Tool" },
    { imageUrl: "ManagingTools.png", imageText: "Managing Tool" },
    { imageUrl: "LightingExtensionCords.png", imageText: "Lighting & Extension Cords" },
    { imageUrl: "LeatherGoods.png", imageText: "Leather Goods" },
    { imageUrl: "MarkingProducts.png", imageText: "Making Products" },
    { imageUrl: "SandingTools.png", imageText: "Sanding Tools" },
    { imageUrl: "VacuumsAccessories.png", imageText: "vacuums/Accessories" },
    { imageUrl: "Scaffoldingbenches.png", imageText: "Scaffolding/benches" },
    { imageUrl: "PowerTools.png", imageText: "Power Tolls" },
  ];

  return (
    <Box>
      <Box className="container py-5">
        <Box className="d-flex justify-content-center ">
          <Box className="text-center">
            <Image src={`/crown.png`} width={100} height={46} />
            <Text size="h3" weight="bold" transform="uppercase">
              Departments
            </Text>
          </Box>
        </Box>
        <Box className="row row-cols-1 row-cols-md-6 mt-4">
          {departmentArray.map((depItem) => {
            return (
              <Box className="col">
                <Box className="department-container">
                    <Box className="text-center pointer department-card">
                    {/* <Image width={300} height={300} src={`/zanoKits.png`} alt="logo" /> */}
                    <img
                        className="img-fluid"
                        src={depItem.imageUrl}
                        alt="zanoKits"
                    />
                    </Box>
                </Box>
                <Text size="h6" className="text-center pt-2" color="primary">{depItem.imageText}</Text>
              </Box>
            );
          })}
        </Box>
        <Box className="d-flex justify-content-center mt-3">
          <Button>View More</Button>
        </Box>
      </Box>
    </Box>
  );
}
