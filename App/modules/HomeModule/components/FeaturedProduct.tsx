import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import Image from "next/image";

export default function Featuredproduct() {
  return (
    <>
      <Box>
        <Box className="container">
          <Box className="row">
            <Box className="d-flex justify-content-center ">
              <Box className="text-center">
                <Image src={`/crown.png`} width={100} height={46} />
                <Text size="h3" weight="bold" transform={"uppercase"}>
                  Featured Products
                </Text>
              </Box>
            </Box>
          </Box>
          <Box className="row">
            
          </Box>
        </Box>
      </Box>
    </>
  );
}
