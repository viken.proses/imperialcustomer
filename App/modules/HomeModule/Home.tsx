import Layout from "../../shared/components/Layout";
import Department from "./components/Departments";
import Featuredproduct from "./components/FeaturedProduct";
import Slider from "./components/Slider";
import Topseller from "./components/Topseller";

export default function Home(){
return(
    <Layout>
        <Slider/>
        <Department/>
        <Topseller/>
        <Featuredproduct/>
    </Layout>
)
}