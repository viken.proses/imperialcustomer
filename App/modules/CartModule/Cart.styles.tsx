import Box from "App/ui/Box/Box";
import { styled } from "App/theme/stitches.config";
import Card from "@ui/Card";

export const BannerItem = styled(Box, {
  '.sliderTextTitle': {
    fontSize: "$h2",
  },
})

export const ProductList = styled(Box, {
  borderTop: 1,
  borderTopStyle: "solid",
  borderTopColor: "$gbg3",
  py: "$10",
  '&.cart_row_disable': {
    background: '#eee',
    pointerEvents: 'none'
  },
  '.col': {
    textAlign: "center",
    '.img-fluid':{
      height: 60,
      objectFit: "contain"
    }
  }
})

export const ProductCard = styled(Card, {
  width: '100%',
  minHeight: 300,
  marginTop: '$15',
  // marginBottom: '$15',
  marginLeft: "auto",
  marginRight: "auto"
})




export const DropdownGroup = styled(Box, {
  display: "flex",
  gap: "$5"
})
export  const TriggerMenu = styled('a', {

})
export  const TriggerButton = styled('button', {

    all: "unset",
    fontFamily: "inherit",
    // borderRadius: "100%",
    display: "inline-block",
    alignItems: "center",
    justifyContent: "center",
    color: "$pText1",
    $$bs: "$colors$indigo5",
    "&:hover": { 
      // backgroundColor: "$bgHover",
      opacity: 0.9 
    },
    "&:focus": { 
      // backgroundColor: "$bgActive" 
    },
    variants: {
      size: {
        user: {
          width: 24
        },
      },
      color: { 
        default: {
          backgroundColor: "$indigo9",
          color: "$whiteA12"
        },
        gradient: {
          backgroundImage: "$gradient",
          color: "$whiteA12"
        },
        user: {
          backgroundColor: "#ff0000",
          color: "$whiteA12"
        },
        clear: {
          backgroundColor: "transparent",
          color: "$whiteA12"
        },
      },
      rounded: {
        true: {
          br: '$sm'
        }
      },
    }
  })