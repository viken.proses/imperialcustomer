import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React from "react";
import Quantityhand from "App/shared/components/ProductCard/component/PlusMinus";
import styles from "../Cart.module.scss";
import { Button } from "@ui/Button/Button";

let product = [1, 2];

function CartProduct() {
  return (
    <Box className="row">
      <Box className="col col-8">
        <Box className={`row mx-0 p-2 ${styles.productHeadingsbg}`}>
          <Box className="col col-7 col-md-7 text-start">
            <Text size="h4" font="bold" className="mt-0">
              Product
            </Text>
          </Box>
          <Box className="col col-md-2 text-start">
            <Text size="h4" font="bold" className="mt-0">
              Qty.
            </Text>
          </Box>
          <Box className="col col-md-2 text-end">
            <Text size="h4" font="bold" className="mt-0">
              Price
            </Text>
          </Box>
          <Box className="col col-md-1 text-end"></Box>
        </Box>

        {product.map(() => {
          return (
            <Box
              className={`row mx-0 ${styles.ProductsListingCart} align-items-center`}
            >
              <Box className="col col-7 col-md-7 text-start mt-md-0">
                <Box className="d-flex align-items-center">
                  <Box className={`${styles.cartProductImage} p-2`}>
                    <img className="img-fulid" src={"/TAJIMAChalkBox.png"} />
                  </Box>
                  <Text size="h6" font="bold" className="mt-0 mx-2">
                    TAJIMA Chalk Box
                  </Text>
                </Box>
              </Box>
              <Box className="col col-2 col-md-2">
                <Quantityhand />
              </Box>
              <Box className="col col-2 col-md-2 text-end" name="price">
                <Text size="h5" font="bold" className="mt-0">
                  ₹ 200
                </Text>
              </Box>
              <Box className="col col-md-1 text-start">
                <Text font="bold">X</Text>
              </Box>
            </Box>
          );
        })}
        <Box className="text-end pt-5">
          <Button color="secoundry" rounded size="md" className={`mx-2 py-1 px-5  ${styles.cartbuttons}`} >Update Cart</Button>
          <Button color="secoundry" rounded size="md" className={`py-1 px-3  ${styles.cartbuttons}`}>Continue Shopping</Button>
        </Box>
      </Box>
      <Box className={`col col-4`}>
        <Box className="boxShadow">
          <Box className={`${styles.productHeadingsbg} p-2`}>
            <Text size="h3" font="bold" className="mt-0 text-center">
              Cart Total
            </Text>
          </Box>
          <Box className={`p-3 ${styles.productCartTotalBox}`}>
            <Box className={`d-flex pb-3 ${styles.bottomBorder}`}>
              <Text className="flex-grow-1">Subtotal</Text>
              <Text>$200</Text>
            </Box>
            <Box className={`d-flex pb-4 pt-2 ${styles.bottomBorder}`}>
              <Text className="flex-grow-1">Tax</Text>
              <Text>$0.00</Text>
            </Box>
            <Box className={`d-flex pb-3 pt-2 ${styles.bottomBorder}`}>
              <Text className="flex-grow-1">Total</Text>
              <Text
                font="bold"
                size="h3"
                className={`${styles.textblack}`}
              >
                $200
              </Text>
            </Box>
            <Box className="pt-5 pb-1">
              <Button full className={`py-1 ${styles.cartbuttons}`} rounded>
                Shipping & Payment
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}

export default CartProduct;
