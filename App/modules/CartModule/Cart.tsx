// import { addUser } from 'App/api/user';
import Box from "@ui/Box/Box";
import Card from "@ui/Card";
import Layout, { toastAlert } from "App/shared/components/Layout";
import React from "react";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";

import CartProduct from "./component/CartProduct";

// localStorage.getItem(TOKEN_PREFIX),

function Cart() {
  return (
    <Layout>
      {
        <Box className="container pt-5">
              <Text size="h2" font="bold" className="pb-4">
                Shopping Cart
              </Text>
          <CartProduct />
        </Box>
      }
    </Layout>
  );
}

export default Cart;
