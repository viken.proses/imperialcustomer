export type RegistrationFormData={
    firstName: string,
  lastName:string,
  email: string,
  gender:"male" | "female",
  password: string,
  comfirmPassword:string,
  mobileNumber:number | null,
  month:string | null,
  year:number | null,
  date:number | null,
  address:string,
}