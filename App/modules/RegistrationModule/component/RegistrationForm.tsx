import React from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import Box from "App/ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Input from "App/ui/Input";
import InputPassword from "@ui/InputPassword";
import { Label } from "@radix-ui/react-dropdown-menu";
import Text from "@ui/Text/Text";
import { RegistrationFormData } from "../RegistrationTypes";
import styles from "../Registration.module.scss";
import Radio from "@ui/Radio/Radio";
import { RadioGroup } from "@radix-ui/react-radio-group";
import SelectMenu from "@ui/Select/Select";

const ValidationSchema = Yup.object().shape({
  email: Yup.string()
    .required("Email is required")
    .email("Please Enter Valid Email")
    .matches(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      "Please Enter Valid Email"
    ),
  password: Yup.string()
    .required("Password should have atleast 6 characters.")
    .min(6, "Password should have atleast 6 characters."),
});

let initialFormVales: RegistrationFormData = {
  firstName: "",
  lastName: "",
  email: "",
  gender: "female",
  password: "",
  comfirmPassword: "",
  mobileNumber: null,
  month: null,
  year:null,
  date:null,
  address: "",
};


let mmonthArray=[{value:"January",label:"January"},{value:"February",label:"February"},
{value:"March",label:"March"},	{value:"April",label:"April"},
{value:"May",label:"May"},	{value:"June",label:"June"},
{value:"July",label:"July"},	{value:"August",label:"August"},
{value:"September",label:"September"},	{value:"October",label:"October"},
{value:"November",label:"November"},	{value:"December",label:"December"}]

let dateArray:any=[]
for(let i=1;i<=31;i++){
dateArray.push({value:i,label:i});
}

let yearArray:any=[]
for(let i=1945;i<=2022;i++){
  yearArray.push({value:i,label:i});
}


function RegistrationForm() {
  return (
    <Box className={`${styles.RegistrationFormbg} p-5`}>
      <Formik
        initialValues={initialFormVales}
        validationSchema={ValidationSchema}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          errors,
          touched,
        }) => (
          <>
            {/* Name input                    */}
            <Box className="row">
              <Box className="col col-2 px-0">
                <Label className="text-end ">
                  <Text size="h6" font="bold" className="pt-2">
                    Your Name
                  </Text>
                </Label>
              </Box>
              <Box className="col col-5">
                <Input
                  required
                  onChange={handleChange}
                  name="firstName"
                  value={values.firstName}
                  error={errors.firstName}
                  onBlur={handleBlur}
                />
              </Box>
              <Box className="col col-5">
                <Input
                  required
                  onChange={handleChange}
                  name="lastName"
                  value={values.lastName}
                  error={errors.lastName}
                  onBlur={handleBlur}
                />
              </Box>
            </Box>
            {/* email input                  */}
            <Box className="row">
              <Box className="col col-2 px-0">
                <Label className="text-end ">
                  <Text size="h6" font="bold" className="pt-2">
                    email
                  </Text>
                </Label>
              </Box>
              <Box className="col col-10">
                <Input
                  required
                  onChange={handleChange}
                  name="email"
                  value={values.email}
                  error={errors.email}
                />
              </Box>
            </Box>
            {/* Gender input                  */}
            <Box className="row">
              <Box className="col col-2 px-0">
                <Label className="text-end ">
                  <Text size="h6" font="bold" className="pt-2">
                    Gender
                  </Text>
                </Label>
              </Box>
              <Box className="col col-10">
                <RadioGroup className={`d-flex ${styles.genderRadio}`}>
                <Radio label="Female"/>
                <Radio label="Male"/>
                </RadioGroup>
              </Box>
            </Box>
            {/* Password input                  */}
            <Box className="row">
              <Box className="col col-2 px-0">
                <Label className="text-end ">
                  <Text size="h6" font="bold" className="pt-2">
                    Password
                  </Text>
                </Label>
              </Box>
              <Box className="col col-4">
                {" "}
                <InputPassword
                  required
                  onChange={handleChange}
                  name="password"
                  value={values.password}
                  error={errors.password}
                />
              </Box>
              <Box className="col col-2 px-0">
                <Label className="text-end">
                  <Text size="h6" font="bold" className="pt-2" >
                    Comfirm Password
                  </Text>
                </Label>
              </Box>
              <Box className="col col-4">
                {" "}
                <InputPassword
                  required
                  onChange={handleChange}
                  name="comfirmPassword"
                  value={values.comfirmPassword}
                  error={errors.comfirmPassword}
                />
              </Box>
            </Box>
            {/* Phone input                  */}
            <Box className="row">
              <Box className="col col-2 px-0">
                <Label className="text-end ">
                  <Text size="h6" font="bold" className="pt-2">
                    Phone Number
                  </Text>
                </Label>
              </Box>
              <Box className="col col-10">
                <Input
                  required
                  onChange={handleChange}
                  name="mobileNumber"
                  value={values.mobileNumber}
                  error={errors.mobileNumber}
                />
              </Box>
            </Box>
            {/* Date of birth input                  */}
            <Box className="row">
            <Box className="col col-2 px-0">
                <Label className="text-end ">
                  <Text size="h6" font="bold" className="pt-2">
                    Birth Date
                  </Text>
                </Label>
              </Box>
              <Box className="col d-flex">
              <SelectMenu
              className={styles.formMonth}
                  onChange={(e: any) => {
                   console.log(e, "e")
                   setFieldValue("month", e.value)
                  }}
                  name="month"
                  items={mmonthArray}
                  bindName="value"
                  bindValue="value"
                  placeholder="Month"
                  value={values.month}
                  required
                  // error={submitAttempt && errors.month}
                />
                <SelectMenu 
                  onChange={(e: any) => {
                   console.log(e, "e")
                   setFieldValue("date", e.value)
                  }}
                  className={`px-2 ${styles.formDate}`}
                  name="date"
                  placeholder="Day"
                  items={dateArray}
                  bindName="value"
                  bindValue="value"
                  value={values.date}
                  required
                  // error={submitAttempt && errors.month}
                />
                <SelectMenu
                  onChange={(e: any) => {
                   console.log(e, "e")
                   setFieldValue("year", e.value)
                  }}
                  className={styles.formYear}
                  placeholder="Year"
                  name="year"
                  items={yearArray}
                  bindName="value"
                  bindValue="value"
                  value={values.year}
                  required
                  // error={submitAttempt && errors.month}
                />
              </Box>
            </Box>
            {/* Address input                  */}
            <Box className="row">
              <Box className="col col-2 px-0">
                <Label className="text-end ">
                  <Text size="h6" font="bold" className="pt-2">
                    Where you live
                  </Text>
                </Label>
              </Box>
              <Box className="col col-10">
                <Input
                  required
                  onChange={handleChange}
                  name="mobileNumber"
                  value={values.mobileNumber}
                  error={errors.mobileNumber}
                  onBlur={handleBlur}
                />
              </Box>
            </Box>
            {/* Buttons input                  */}
            <Box className="row">
              <Box className="col col-4"></Box>
              <Box className="col col-4 text-end">
                <Button
                full
                  color="default"
                  className="py-2 my-2"
                  onClick={(e: any) => {
                    handleSubmit(e);
                  }}
                >
                  <Text
                    className=" py-1"
                    color="light"
                    size="h6"
                    font="semiBold"
                  >
                    Allready a user?Login
                  </Text>
                </Button>
              </Box>
              <Box className="col-4">
              <Button
              full
                  color="default"
                  className=" py-2 my-2"
                  onClick={(e: any) => {
                    handleSubmit(e);
                  }}
                >
                  <Text
                    className=" py-1"
                    color="light"
                    size="h6"
                    font="semiBold"
                  >
                    Create Account
                  </Text>
                </Button>
              </Box>
            </Box>
          </>
        )}
      </Formik>
    </Box>
  );
}

export default RegistrationForm;
