import Layout from "App/shared/components/Layout";
import Box from "@ui/Box/Box";
import Sidemenu from "./component/sidemenu";
import RegistrationForm from "./component/RegistrationForm";

function Registration(){
  return (
    <Layout>
    <Box className="container py-4">
        <Box className="row ">
            <Box className="col clo-md-3 d-none d-sm-none d-md-block">
                <Sidemenu />
            </Box>
            <Box className={`col col-md-9`}>
                <RegistrationForm />
            </Box>
        </Box>
    </Box>
</Layout>
  );
};

export default Registration;
