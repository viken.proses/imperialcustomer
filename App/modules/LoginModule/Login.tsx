import LoginForm from "./component/LoginForm";
import Layout from "App/shared/components/Layout";
import Box from "@ui/Box/Box";
import Sidemenu from "./component/Sidemenu";
import { useState } from "react";


function LoginModule(){
    
const [submitAttempt, setsubmitAttempt] = useState<boolean>(false);

  return (
    <Layout>
    <Box className="container py-4">
        <Box className="row ">
            <Box className="col clo-md-3 d-none d-sm-none d-md-block">
                <Sidemenu/>
            </Box>
            <Box className={`col col-md-9`}>
                <LoginForm
                setsubmitAttempt={setsubmitAttempt}
                submitAttempt={submitAttempt}/>
            </Box>
        </Box>
    </Box>
</Layout>
  );
};

export default LoginModule;
