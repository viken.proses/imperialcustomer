import React from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import Box from "App/ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Input from "App/ui/Input";
import InputPassword from "@ui/InputPassword";
import { Label } from "@radix-ui/react-dropdown-menu";
import Text from "@ui/Text/Text";
import { LoginFormData } from "../LoginTypes";
import styles from "../Login.module.scss";

const ValidationSchema = Yup.object().shape({
  email: Yup.string()
    .required("Email is required")
    .email("Please Enter Valid Email")
    .matches(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      "Please Enter Valid Email"
    ),
  password: Yup.string()
    .required("Password should have atleast 6 characters.")
    .min(6, "Password should have atleast 6 characters."),
});

let initialFormVales: LoginFormData = {
  email: "",
  password: "",
};
type Formtype={
    setsubmitAttempt : any;
  submitAttempt : boolean;
}
function LoginForm({setsubmitAttempt,submitAttempt}:Formtype) {
  return (
    <Box className={`${styles.loginFormbg} pt-5 pb-5`}>
      <Formik
        initialValues={initialFormVales}
        validationSchema={ValidationSchema}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          errors,
          touched,
        }) => (
          <>
            <Box className="row">
              <Box className="col col-4">
                <Label className="text-end">Email</Label>
              </Box>
              <Box className="col col-4">
                <Input
                  required
                  onChange={handleChange}
                  name="email"
                  value={values.email}
                  error={submitAttempt && errors.email}
                  onBlur={handleBlur}
                />
              </Box>
            </Box>
            <Box className="row">
              <Box className="col col-4">
                <Label className="text-end">Password</Label>
              </Box>
              <Box className="col col-4">
                {" "}
                <InputPassword
                  required
                  onChange={handleChange}
                  name="password"
                  value={values.password}
                  error={submitAttempt && errors.password}
                  onBlur={handleBlur}
                />
              </Box>
            </Box>
            <Box className="row"> 
              <Box className="col col-4"></Box>
              <Box className="col col-4">
                <Button
                  color="default"
                  className="px-5 py-2 my-2"
                  onClick={(e: any) => {
                    handleSubmit(e);
                    setsubmitAttempt(true);
                  }}
                >
                  <Text
                    className="px-4 py-1"
                    color="light"
                    size="h6"
                    font="bold"
                  >
                    Login
                  </Text>
                </Button>
                <Box>
                  <Text color="primary" className="py-2" size="h6" font="bold">
                    Forgot Password? Click her to reset
                  </Text>
                </Box>
              </Box>
            </Box>
          </>
        )}
      </Formik>
    </Box>
  );
}

export default LoginForm;
