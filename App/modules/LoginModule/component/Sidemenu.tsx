import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";

export default function Sidemenu() {
  return (
    <>
      <Box className="p-3 boxShadow">
        <Text size="h5" font="bold" className="py-2">
          Don't have an account?
        </Text>
        <Text className="py-2">you can change or add more information</Text>
        <Text className="py-2">Recive Promotional Information and offers</Text>
        <Text className="py-2">
          Download your invoices even when you buy from our local store
        </Text>
        <Text className="py-2">Create jobs and buy your products specific to your jobs</Text>
        <Text className="py-2">
          Manage your multiple address, so we can deliver your items directly to
          your building sites
        </Text>
        <Button className="py-1" full color="secoundry"><Text className="py-2" size="h6" font="bold">Create an account</Text> </Button>
      </Box>
    </>
  );
}
