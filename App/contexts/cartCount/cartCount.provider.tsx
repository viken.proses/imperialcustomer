import React, { useReducer } from 'react';
import  CartCountContext  from "./cartCount.context"

const isBrowser = typeof window !== 'undefined';

const INITIAL_STATE = {
//@ts-ignore
  count: isBrowser && localStorage.getItem('user_data') ? JSON.parse(localStorage.getItem('user_data')).cartCount : 0
};

function reducer(state:any, action:any) {

  let customerInfo:any = {};

  if(localStorage.getItem('user_data')){
    //@ts-ignore
    customerInfo = JSON.parse(localStorage.getItem('user_data'));
  }

  const { type, payload } = action;
  
  switch (type) {

    case "INITIAL_SET_ITEM":
      customerInfo.cartCount = payload;
      localStorage.setItem("user_data", JSON.stringify(customerInfo));
      return { count: payload }

    case "ADD_ITEM":
      customerInfo.cartCount = state.count + 1;
      localStorage.setItem("user_data", JSON.stringify(customerInfo));
      return { count: state.count + 1 };

    case "REMOVE_ITEM":
      customerInfo.cartCount = state.count - 1;
      localStorage.setItem("user_data", JSON.stringify(customerInfo));
      return { count: state.count - 1 };
    
    case "EMPTY_ITEMS":
      customerInfo.cartCount = 0;
      return { count: 0 }

    default:
      return state;
  }

}

export const CartCountProvider = ({ children }:any) => {
  const [cartCountState, cartCountDispatch] = useReducer(reducer, INITIAL_STATE);
  return (
    <CartCountContext.Provider value={{ cartCountState, cartCountDispatch }}>
    {children}
  </CartCountContext.Provider>
  );
};