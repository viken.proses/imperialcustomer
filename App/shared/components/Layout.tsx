import Header from "./Header/Header";
import React, { FC } from "react";
import { toast, ToastContainer } from "react-toastify";
import Footer from "./Footer/Footer";
// method for alert();
export const toastAlert = (type: string, msg: string) => {
  if (type == "success") return toast.success(msg);
  else if (type == "warn") return toast.warn(msg);
  else if (type == "error") return toast.error(msg);
  else return toast.info("An Alert Problem");
};

const Layout: FC = ({ children }) => {
  return (
    <>
      <Header />
      <div className="wrapper">
        {/* <Sidebar /> */}
        <div id="content">{children}</div>
        {/* use for alert */}
        <ToastContainer
          position="top-center"
          autoClose={1500}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
      <Footer />
    </>
  );
};

export default Layout;
