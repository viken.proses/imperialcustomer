import UiContext from "../../../contexts/ui/ui.context";
import { useRouter, NextRouter } from "next/router";
import ListIcon from "../../../icons/ListIcon";
import LogoutIcon from "../../../icons/LogoutIcon";
import UserIcon from "../../../icons/UserIcon";
import clsx from "clsx";
import styles from "./Header.module.scss";
import { Button } from "../../../ui/Button/Button";
import DropdownStyled from "../../../ui/Dropdown/Dropdown";
import Text from "../../../ui/Text/Text";
import {
  DropdownGroup,
  MainMenu,
  TriggerButton,
  TriggerMenu,
} from "./Header.styles";
import { memo, useContext, useEffect, useState } from "react";
import Lock from "../../../icons/Lock";
import Link from "next/link";
import CartIcon from "../../../icons/CartIcon";
import RightArrow from "../../../icons/RightArrow";
import Box from "../../../ui/Box/Box";
import { MENU_LIST } from "../../../utils/constants";
import CartCountContext from "../../../contexts/cartCount/cartCount.context";
import Logo from "./Logo";
import TopHeader from "./components/TopHeader";
import HeaderMenu from "./components/BottomHeaderMenu";
import MiddleHeader from "./components/MiddleHeader";

//action type for dropdown
export type actionTypes = {
  name: "My Profile" | "My Orders" | "Change Password" | "Logout";
  id: number;
  icon?: any;
};

//app actions
export const ACTIONS: actionTypes[] = [
  { name: "My Profile", id: 1, icon: <UserIcon /> },
  { name: "My Orders", id: 4 , icon: <CartIcon />},
  { name: "Change Password", id: 2, icon: <Lock /> },
  { name: "Logout", id: 3, icon: <LogoutIcon /> },
];

export const KITS: any = [
  { name: "kit 1", id: 1, icon: <RightArrow /> },
  { name: "kit 2", id: 2, icon: <RightArrow /> },
  { name: "kit 3", id: 3, icon: <RightArrow /> },
];

function Header(props: any) {
  const { cartCountState, cartCountDispatch }: any =
    useContext(CartCountContext);
  const { uiState, uiDispatch } = useContext(UiContext);
  const router: NextRouter = useRouter();
  const [title, setTitle] = useState<string>("Admin");
  const [activeLink, setActiveLink] = useState<string>("Home");

  const [customerInfo, setCustomerInfo] = useState<any>({});

  useEffect(() => {
    if (localStorage.getItem("user_data")) {
      //@ts-ignore
      setCustomerInfo(JSON.parse(localStorage.getItem("user_data")));
      console.log(router.pathname)
      setActiveLink(router.pathname)
    }

    return () => {};
  }, []);

  const handleChange = (id: string | number) => {
    switch (id) {
      case 1:
        adminProfile();
        break;
      case 2:
        ChangePassword();
        break;
      case 3:
        logout();
        break;
      case 4:
        MyOrders();
        break;
      default:
        break;
    }
  };

  const logout = () => {
    localStorage.clear();
    cartCountDispatch({ type: "EMPTY_ITEMS" });
    router.push("/Login");
  };

  const adminProfile = () => {
    router.push("/Profile");
  };

  const ChangePassword = () => {
    router.push("/ChangePass");
  };

  const MyOrders = () => {
    router.push("/MyOrders");
  }; 


  const activeHandle = (title:string, link: string) => {
    setActiveLink(link)
    router.push(link);
  }

  return (
    <header
      className={clsx(styles.header, {
        [styles.headerClose]: !uiState.sidebarOpen,
      })}
    >
      <TopHeader />
      <MiddleHeader />
      <HeaderMenu />
      {/* <Box className={`${styles.headerContainer}`}>
        <Button
          color={"clear"}
          onClick={() => uiDispatch({ type: "TOGGLE_SIDEBAR" })}
        >
          <ListIcon stroke={"black"} />
        </Button>
        <Box className={`${styles.headerContainerCenter}`}>
          <MainMenu className={`d-none d-md-flex ${styles.leftMenu}`}>

            <Text className={`${styles.menuLink}  ${activeLink == MENU_LIST.HOME.link ? styles.active : '' }`} onClick={()=>activeHandle(MENU_LIST.HOME.title, MENU_LIST.HOME.link)}>{MENU_LIST.HOME.title}</Text>

            <Text className={`${styles.menuLink}  ${activeLink == MENU_LIST.SECURITY_KITS.link ? styles.active : '' }`} onClick={()=>activeHandle(MENU_LIST.SECURITY_KITS.title, MENU_LIST.SECURITY_KITS.link)}>{MENU_LIST.SECURITY_KITS.title}</Text>

            <Text className={`${styles.menuLink}  ${activeLink == MENU_LIST.PRODUCTS.link ? styles.active : '' }`} onClick={()=>activeHandle(MENU_LIST.PRODUCTS.title,  MENU_LIST.PRODUCTS.link)}>{MENU_LIST.PRODUCTS.title}</Text>
            
          </MainMenu>

          <Logo />
          
          
        </Box>

        {customerInfo?.id ? (
          <>
            <Box className="d-flex">
              <Box className="d-flex align-items-center me-3">
                <DropdownGroup>
                  <Link href="/Cart/">
                    <TriggerButton
                      css={{ position: "relative" }}
                      aria-label="Customise options"
                    >
                      <CartIcon color={"black"} />
                      {cartCountState.count > 0 ? (
                        <span className="cartCount">
                          {cartCountState.count}
                        </span>
                      ) : null}
                    </TriggerButton>
                  </Link>
                </DropdownGroup>
              </Box>

              <Box className="d-flex align-items-center me-0">
                <DropdownGroup>
                  <DropdownStyled
                    className="headerDropdown"
                    arr={ACTIONS}
                    trigger={
                      <TriggerButton
                        css={{ position: "relative" }}
                        aria-label="Customise options"
                      >
                        <UserIcon color={"black"} />
                      </TriggerButton>
                    }
                    triggerProps={{ asChild: true }}
                    onSelect={(e: any) => handleChange(e.id)}
                  />
                </DropdownGroup>
              </Box>
            </Box>
          </>
        ) : (
          <MainMenu className={`d-none d-md-flex ${styles.leftMenu}`}>
            <Link href={`/Registration`}>Sign Up</Link>/
            <Link href={`/Login`}>Login</Link>
          </MainMenu>
        )}
      </Box> */}
    </header>
  );
}

export default memo(Header);
