import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import { MainMenu } from "../Header.styles";
import styles from "../Header.module.scss";

export default function HeaderMenu() {
  return (
    <>
      <Box className={`d-none d-md-flex py-2 ${styles.headerBottomMenuBg} headingShadow`}>
        <Box className="container ">
          <MainMenu className={`d-md-flex`}>
            <Text color="primary" className="MainMenuLinks me-3">Home</Text>
            <Text color="primary" className="MainMenuLinks mx-3">Department</Text>
            <Text color="primary" className="MainMenuLinks mx-3">Brands</Text>
            <Text color="primary" className="MainMenuLinks mx-3">Parts</Text>
            <Text color="primary" className="MainMenuLinks mx-3">Sale!</Text>
          </MainMenu>
        </Box>
      </Box>
    </>
  );
}
