import Text from "@ui/Text/Text";
import Box from "@ui/Box/Box";
import { MainMenu } from "../Header.styles";
import styles from "../Header.module.scss";


export default function TopHeader() {
  return (
    <Box className={`d-none d-md-flex ${styles.topbg} py-1`}>
        <Box className="container">
            <Box className="row py-2">
                <Box className="col">
                    <Text color="light" font="extraLight" size="h6">welcome to our store Call +78 999 443 for support</Text>
                </Box>
                <Box className="col text-right">
                    <MainMenu className={` d-md-flex justify-content-end`}>
                        <Text className="topMenuLinks mx-3" color="light" size="h6" font="extraLight" >STORE</Text>
                        <Text className="topMenuLinks mx-3" color="light" size="h6" font="extraLight" >LOCATOR</Text>
                        <Text className="topMenuLinks mx-3" color="light" size="h6" font="extraLight" >FAQS</Text>
                        <Text className="topMenuLinks mx-3" color="light" size="h6" font="extraLight" >CONTACT US</Text>
                        <Text className="topMenuLinks mx-3" color="light" size="h6" font="extraLight" >LOGIN</Text>
                        <Text className="topMenuLinks mx-3" color="light" size="h6" font="extraLight" >SIGN UP</Text>
                    </MainMenu>
                </Box>
            </Box>
        </Box>
    </Box>
  );
}
