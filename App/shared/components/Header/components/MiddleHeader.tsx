import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Input from "@ui/Input";
import Text from "@ui/Text/Text";
import CartIcon from "App/icons/CartIcon";
import ListIcon from "App/icons/ListIcon";
import UserIcon from "App/icons/UserIcon";
import Logo from "../Logo";

export default function MiddleHeader() {
  return (
    <Box className="py-1">
      <Box className="container">
        <Box className="row align-items-center">
          <Box className="col d-flex">
            
        <Button
          className="d-md-none"
          color={"clear"}
          onClick={() => uiDispatch({ type: "TOGGLE_SIDEBAR" })}
        >
          <ListIcon stroke={"black"} />
        </Button>
            <Logo />
          </Box>
          <Box className="col d-none d-md-flex justify-content-md-end">
            <Input />
          </Box>
          <Box className="col d-flex justify-content-end">
            <Box className="text-center mx-3">
                <UserIcon size="20"/>
                <Text size="h6">Log in</Text>
            </Box>
            <Box className="text-center">
                <CartIcon size="20"/>
                <Text size="h6">My Cart</Text>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
