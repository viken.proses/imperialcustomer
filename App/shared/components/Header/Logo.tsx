import Box from '../../../ui/Box/Box';
import Link from 'next/link';
import Image from "next/image";
import React from 'react';
import styles from "./Header.module.scss";

function Logo() {
    return (

        <Box className={`${styles.logoSection}`}>
            <Link href={`/`}>
                <a>
                    <Image width={150} height={52} src={`/logo.png`} alt="logo" />
                </a>
            </Link>
        </Box>

    )
}

export default React.memo(Logo)
