import { useRouter, NextRouter } from "next/router";
// import clsx from "clsx";
import styles from "App/shared/components/Footer/Footer.module.scss";
import Image from "next/image";
import {
  DropdownGroup,
  MainMenu,
  TriggerButton,
  TriggerMenu,
} from "./Footer.styles";
import { useContext, useState } from "react";
import Link from "next/link";
import Text from "../../../ui/Text/Text";
import Input from "../../../ui/Input";
import { Button } from "../../../ui/Button/Button";
import UiContext from "../../../contexts/ui/ui.context";
import Facebook from "App/icons/Facebook";
import Instagram from "App/icons/Instagram";

//action type for dropdown

function Footer(props: any) {
  const { uiState, uiDispatch } = useContext(UiContext);
  const router: NextRouter = useRouter();
  const [title, setTitle] = useState<string>("Admin");

  const handleChange = (id: string | number) => {
    switch (id) {
      case 1:
        adminProfile();
        break;
      case 3:
        logout();
        break;
      case 2:
        ChangePassword();
        break;
      default:
        break;
    }
  };

  const logout = () => {
    localStorage.clear();
    router.push("/Login");
  };

  const adminProfile = () => {
    router.push("/Profile");
  };

  const ChangePassword = () => {
    router.push("/ChangePass");
  };

  return (
    <>
      <footer className={`${styles.footer}`}>
        {/* <div className={`${styles.footerContainer}`}> */}
        <div className={`container`}>
          <div className={`row row-cols-1 row-cols-md-4`}>
            {/* <Image className={styles.footerLogo} width={91} height={50} src={`/logo.png`} alt="logo" /> */}
            <div className="col">
              <div className={`${styles.footerMenuHeading}`}>
                <h6 className="mt-0 mt-md-0">Our store</h6>
              </div>
              <div className={`${styles.footerMenu}`}>
                <Link href={`/`}>Shop</Link>
                <Link href={`/`}>Shop Locator</Link>
                <Link href={`/`}>Brands</Link>
                <Link href={`/`}>Gift cards</Link>
              </div>
            </div>
            <div className="col">
              <div className={`${styles.footerMenuHeading}`}>
                <h6 className="mt-3 mt-md-0">Customer Service</h6>
              </div>
              <div className={` ${styles.footerMenu}`}>
                <Link href={`/`}>Contact us</Link>
                <Link href={`/`}>Track our order4r</Link>
                <Link href={`/`}>Returns</Link>
                <Link href={`/`}>Pickup & Delivery</Link>
              </div>
            </div>
            <div className="col">
              <div className={`${styles.footerMenuHeading}`}>
                <h6 className="mt-3 mt-md-0">About IWS</h6>
              </div>
              <div className={` ${styles.footerMenu}`}>
                <Link href={`/`}>About Imperial WallBoards Supply</Link>
                <Link href={`/`}>Leadership</Link>
                <Link href={`/`}>Testimonials</Link>
              </div>
            </div>
            <div className="col">
              <div className={`${styles.footerMenuHeading}`}>
                <h6 className="mt-3 mt-md-0">Contact us</h6>
              </div>
              <div className={`${styles.footerMenu}`}>
                <div className={` ${styles.footerIcons} mb-4 mb-md-5`}>
                  <Facebook color="light" /> <Instagram color="light" />
                </div>
                <div className={`${styles.creditCard} d-flex justify-content-center`}>
                  <div className="mx-1">
                    <Image
                      width={43}
                      height={28}
                      src={`/visa.png`}
                      alt="visa"
                    />
                  </div>
                  <div className="mx-1">
                    <Image
                      width={43}
                      height={28}
                      src={`/mastercard.png`}
                      alt="mastercard"
                    />
                  </div>
                  <div className="mx-1">
                    <Image
                      width={43}
                      height={28}
                      src={`/maestro.png`}
                      alt="mestro"
                    />
                  </div>
                  <div className="mx-1">
                    <Image
                      width={43}
                      height={28}
                      src={`/cirrus.png`}
                      alt="cirrus"
                    />
                  </div>
                  <div className="mx-1">
                    <Image
                      width={43}
                      height={28}
                      src={`/american-express.png`}
                      alt="american-express"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* </div> */}
      </footer>
      <div className={`${styles.copyWritebg} py-2`}>
        <div className="container">
          <div className={`row ${styles.footerContainer}`}>
            {/* <Image className={styles.footerLogo} width={91} height={50} src={`/logo.png`} alt="logo" /> */}
            <div className={` col text-center ${styles.footerContainerCenter}`}>
              <Text size="h6">
                Imperial Wall Board© 2022. All Rights Reserved.
              </Text>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Footer;
