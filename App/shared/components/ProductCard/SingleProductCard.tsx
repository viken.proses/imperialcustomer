import Box from "@ui/Box/Box";
import { Button } from "@ui/Button/Button";
import Text from "@ui/Text/Text";
import MinusCircle from "App/icons/MinusCircle";
import PlusCircle from "App/icons/PlusCircle";
import Quantityhand from "./component/PlusMinus";

export default function SingleProductCard({product}:any) {
    {console.log(product)}
  return (
    <Box className="productColContainer boxShadow">
      <Box className="productCard">
        <Box className="p-3">
          <img src={product.img} className="rounded img-fulid" />
        </Box>

        <Box className="text-center productPriceContainer">
          <span className="productPrice">{product.price}</span>
        </Box>

        <Box className="p-3">
          <Text
            transform="uppercase"
            font="bold"
            size="h4"
            className="py-2 text-center"
          >
            {product.productTitle}
          </Text>
          <Text size="h6" className="py-2">
            {product.discription}
          </Text>
          <Box className="d-flex py-2">
            <Quantityhand />
            <Box className="productAddtoCartButton">
              <Button rounded className="addtoCartButton">Add to Cart</Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
