import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import MinusCircle from "App/icons/MinusCircle";
import PlusCircle from "App/icons/PlusCircle";

export default function Quantityhand(){
    return (
        <Box className="productPlusMinusIcons flex-grow-1 d-flex align-items-center">
              <MinusCircle size="25" />
              <Text className="px-2">1</Text>
              <PlusCircle size="25" />
            </Box>
    )
}