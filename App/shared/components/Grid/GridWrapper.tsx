import Box from '@ui/Box/Box'
import Card from '@ui/Card'
import React from 'react'

function GridWrapper({children}: {children: React.ReactNode}) {
  return (
    <Box className="pageContent">
    <Card css={{p:"$5 $0 $5 $0"}}>
{children}
    </Card>
    </Box>
  )
}

export default GridWrapper