import Box from "@ui/Box/Box";
import { styled } from "App/theme/stitches.config";


export const HeadContainer = styled(Box, {
    padding: "20px 30px 10px 30px",
    position: "sticky",
    top: "0px",
    zIndex: 1099,
    bg: "#F5F5F5",
    borderRadius: "10px 10px 0px 0px",
    display:"flex",
    justifyContent: "space-between",
    alignItems: "center"
})