import Box from "@ui/Box/Box";
import Text from "@ui/Text/Text";
import React from "react";
import { HeadContainer } from "./PageHeader.styles";

function PageHeader({
  title,
  rightContent,
}: {
  title: string;
  rightContent: React.ReactNode;
}) {
  return (
    <HeadContainer>
      <Box>
        <Text size="h3">{title}</Text>
      </Box>
      <Box>
      {rightContent}
      </Box>
    </HeadContainer>
  );
}

export default PageHeader;
