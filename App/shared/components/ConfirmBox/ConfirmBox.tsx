
import Popover from '@ui/Popover'
import React from 'react'

import Cross from "App/icons/Cross";

function ConfirmBox({children, trigger, side="top", contentAlign="center", triggerParent="span"}
:{
  children: React.ReactNode, 
  trigger: string|React.ReactNode, 
  side?:"top"|"left"|"bottom"|"right",
  contentAlign?:"center"|"right"|"left",
  triggerParent?: "div"|"span"
}) {
  return (
    <Popover defaultOpen={false}>
    <Popover.Trigger asChild>
      {
        triggerParent == "span" ? <span>{trigger}</span> : <div>{trigger}</div>
      }
    </Popover.Trigger>
    <Popover.Content side={side} contentAlign={contentAlign}>
        <Popover.Close asChild>
            <Cross color="gray" style={{position: 'absolute',right: '3px',top: '3px', fill: '#707070'}} />
        </Popover.Close>
      {children}
    </Popover.Content>
  </Popover>
  )
}

export default ConfirmBox