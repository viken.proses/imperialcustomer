import React from "react";

function PdfWrapper({ children }: any) {
  return (
    <div
      style={{
        visibility: "hidden",
        position: "absolute",
        top: 50,
        left: 50,
        display: "none",
        margin:15
      }}
    >
      {children}
    </div>
  );
}

export default PdfWrapper;
