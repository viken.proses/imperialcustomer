//@ts-nocheck
let configData = {
  development: {
      ApiUrl: "http://localhost:5000",
      
    },
    staging: {
      ApiUrl: "http://prosesindia.in:5000"
    },
    production: {
        ApiUrl: "http://prosesindia.in:5000"
    }
  }

  export default  function (env: any) {
    return configData[env]
  }
