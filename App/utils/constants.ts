import environment from "App/environment";
import configFunc from "./config";
const config = configFunc(environment);

export const NODE_API_URL = config.ApiUrl;

export const TOKEN_PREFIX = `${NODE_API_URL}zano__`;
export const PERM_PREFIX = `zano__PERM__`;
export const MENU_PREFIX = `zano__MENU__`;
export const USER_ID = 'zano_CUSTOMER_ID';
export const USER_NAME = 'zano_CUSTOMER_NAME';
export const USER_EMAIL = 'zano_CUSTOMER_EMAIL';
export const USER_MOBILE = 'zano_CUSTOMER_MOBILE';

export const TOKEN_S = "@_ZANO_JWT@@$$$$";

export const INITIAL_PAGE = 1;
export const INITIAL_LIMIT = 10;

export const REQUIRED_CLASS = "is-invalid";
export const MESSAGES = {
  INVALID_LOGIN: "Invalid credentials, Please try again.",
  ALREADY_EXIST: "Name already exist !",
  SERVER_ERROR: "Something went wrong !",
};

export const ACTIVE_INACTIVE = [
  { name: "Active", value: "1" },
  { name: "Inactive", value: "0" },
];

export const YES_NO = [
  { name: "Yes", value: "1" },
  { name: "No", value: "0" },
];

export const USER_TYPE = [
  // { name: 'Superadmin', value: 'Superadmin' },
  { name: "Admin", value: "Admin" },
  { name: "User", value: "User" },
];

export const PARTY_TYPE = [
  { name: "Supplier", value: "Supplier" },
  { name: "Customer", value: "Customer" },
  { name: "Self", value: "Self" },
  { name: "Production", value: "Production" },
  { name: "Others", value: "Others" },
];

export const TRANSACTION_MODE = {
  //INWARD
  GRN: "GRN",
  Purchase: "Purchase",
  ProductionFloor: "Production Floor",
  TransferIn: "Transfer-In",
  SalesReturn: "Sales Return",
  SamplesIn: "Samples-In",
  OtherInward: "Other Inward",

  //OUTWARD
  Sales: "Sales",
  TransferOut: "Transfer-Out",
  PurchaseReturn: "Purchase Return",
  SamplesOut: "Samples-Out",
  OtherOutward: "Other Outward",

  //ADJUSTMENT
  Repacking: "Repacking",
  Unpacking: "Unpacking",
  Damaged: "Damaged",
  Sampling: "Sampling",
  OpgStock: "OpgStock",
  OtherReason: "Other Reason",
};

export const INWARD_DROPDOWN = [
  { name: TRANSACTION_MODE.GRN, value: TRANSACTION_MODE.GRN },
  { name: TRANSACTION_MODE.Purchase, value: TRANSACTION_MODE.Purchase },
  {
    name: TRANSACTION_MODE.ProductionFloor,
    value: TRANSACTION_MODE.ProductionFloor,
  },
  { name: TRANSACTION_MODE.TransferIn, value: TRANSACTION_MODE.TransferIn },
  { name: TRANSACTION_MODE.SalesReturn, value: TRANSACTION_MODE.SalesReturn },
  { name: TRANSACTION_MODE.SamplesIn, value: TRANSACTION_MODE.SamplesIn },
  { name: TRANSACTION_MODE.OtherInward, value: TRANSACTION_MODE.OtherInward },
];

export const OUTWARD_DROPDOWN = [
  { name: TRANSACTION_MODE.Sales, value: TRANSACTION_MODE.Sales },
  { name: TRANSACTION_MODE.TransferOut, value: TRANSACTION_MODE.TransferOut },
  {
    name: TRANSACTION_MODE.PurchaseReturn,
    value: TRANSACTION_MODE.PurchaseReturn,
  },
  { name: TRANSACTION_MODE.SamplesOut, value: TRANSACTION_MODE.SamplesOut },
  { name: TRANSACTION_MODE.OtherOutward, value: TRANSACTION_MODE.OtherOutward },
];

export const ADJ_DROPDOWN = [
  { name: TRANSACTION_MODE.Repacking, value: TRANSACTION_MODE.Repacking },
  { name: TRANSACTION_MODE.Unpacking, value: TRANSACTION_MODE.Unpacking },
  { name: TRANSACTION_MODE.Damaged, value: TRANSACTION_MODE.Damaged },
  { name: TRANSACTION_MODE.Sampling, value: TRANSACTION_MODE.Sampling },
  { name: TRANSACTION_MODE.OpgStock, value: TRANSACTION_MODE.OpgStock },
  { name: TRANSACTION_MODE.OtherReason, value: TRANSACTION_MODE.OtherReason },
];

export const MOVEMENT = {
  Inward: "Inward",
  Outward: "Outward",
};

export const MOVEMENT_DROPDOWN = [
  { name: MOVEMENT.Inward, value: MOVEMENT.Inward },
  { name: MOVEMENT.Outward, value: MOVEMENT.Outward },
];

export const UOM = {
  NOs: "NOs",
  KGs: "KGs",
  Grams: "Grams",
  Liters: "Liters",
  Dozens: "Dozens",
  MM: "MM",
  Pack: "Pack",
};

export const UOM_DROPDOWN = [
  { name: UOM.NOs, value: UOM.NOs },
  { name: UOM.KGs, value: UOM.KGs },
  { name: UOM.Grams, value: UOM.Grams },
  { name: UOM.Liters, value: UOM.Liters },
  { name: UOM.Dozens, value: UOM.Dozens },
  { name: UOM.MM, value: UOM.MM },
  { name: UOM.Pack, value: UOM.Pack },
];

export const MODULE_PREFIX = {
  product: "PRO/",
  qrLabel: "QR/",
  stockAdj: "SA/",
  stockInw: "SI/",
  stockOut: "SO/",
};

/***
 * @START IMAGES OR FILES PATH TO DISPLAY
 */

export const FIX_PATH = {
  // cluster: `${NODE_API_URL}/static/cluster/`,
};

/***
 * @END IMAGES OR FILES PATH TO DISPLAY
 */

export const UOM_LIST = [
  { id: "NOs", value: "NOs" },
  { id: "CM", value: "CM" },
  { id: "MM", value: "MM" },
];

export const TYPE_LIST = [
  { id: "Gateway", value: "Gateway" },
  { id: "Independent", value: "Independent" },
  { id: "GPRS", value: "GPRS" },
];

export const TICKETSTATUS_LIST = [
  { id: "Open", value: "Open" },
  { id: "Close", value: "Close" },
  { id: "In Progress", value: "In Progress" },
  { id: "Waiting For New Device", value: "Waiting For New Device" },
  { id: "Reject", value: "Reject" },
];

export const TICKETPRIORITY_LIST = [
  { id: "Low", value: "Low" },
  { id: "Medium", value: "Medium" },
  { id: "High", value: "High" },
];

export const TICKETTYPE_LIST = [
  { id: "Installation", value: "Installation" },
  { id: "Device Connectivity", value: "Device Connectivity" },
  { id: "Others", value: "Others" },
];


export const PRODUCT_TYPE = {
  Item: "Item",
  Kit: "Kit",
  CustomKit: "CustomKit",
};

export const ADDRESS_TYPE = {
  BILLING: "Billing",
  SHIPPING: "Shipping"
}

export const MENU_LIST = {
  HOME: {title: 'Home', link: '/'},
  SECURITY_KITS: {title:'Security Kits', link: '/SecurityKit'},
  PRODUCTS: {title:'Products', link: '/ProductList'},
  SUPPORT: {title:'Support', link: '/Support'},
  HELP: {title: 'Contact Us', link: '/ContactUs'},
}
