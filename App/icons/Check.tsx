import React from 'react';
import WithIcons from "./withProps";

function Check(props: any) {
    return (
      <svg {...props}
        xmlns="http://www.w3.org/2000/svg" 
        style={props.style}
        width={props.width}
        height={props.height}
        className={`css-tj5bde-Svg ${props.className}`}
        viewBox="0 0 20 20">
              <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"/>

            </svg>
    )
}

export default WithIcons(Check);
