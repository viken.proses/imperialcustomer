import React from 'react';
import WithIcons from "./withProps";

function Pin(props: any) {
  return (
    <svg {...props}
     xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
    className={`bi bi-box ${props.className}`}
    viewBox="0 0 28 38">
      <path id="Path_39" data-name="Path 39" d="M17.338,2A13.338,13.338,0,0,0,4,15.338c0,9,11.754,19.173,12.254,19.606a1.667,1.667,0,0,0,2.167,0C19,34.51,30.675,24.34,30.675,15.338A13.338,13.338,0,0,0,17.338,2Zm0,29.426c-3.551-3.334-10-10.52-10-16.088a10,10,0,1,1,20.006,0C27.341,20.906,20.889,28.108,17.338,31.426Zm0-22.757a6.669,6.669,0,1,0,6.669,6.669A6.669,6.669,0,0,0,17.338,8.669Zm0,10a3.334,3.334,0,1,1,3.334-3.334A3.334,3.334,0,0,1,17.338,18.672Z" transform="translate(-4 -2)" fill="#0463ef"/>
    </svg>
  )
}

export default WithIcons(Pin);