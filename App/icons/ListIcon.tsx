import React from "react";
import WithIcons from "./withProps";

function ListIcon(props: any) {
  return (
    // <svg {...props}
    //   xmlns="http://www.w3.org/2000/svg"
    //   style={props.style}
    //   width={props.width}
    //   height={props.height}
    //   className={`bi bi-list ${props.className}`}
    //         viewBox="0 0 16 16"
    //       >
    //         <path
    //           fillRule="evenodd"
    //           d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"
    //         />
    //       </svg>
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi bi-list ${props.className}`}
      viewBox="0 0 16.185 11.119"
    >
      <g id="Icon" transform="translate(1 1)">
        <path
          id="Path"
          d="M0,.429H11.19"
          transform="translate(0 4.131)"
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="2"
        />
        <path
          id="Path-2"
          data-name="Path"
          d="M0,.429H14.185"
          transform="translate(0 -0.429)"
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="2"
        />
        <path
          id="Path-3"
          data-name="Path"
          d="M0,.429H14.185"
          transform="translate(0 8.691)"
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="2"
        />
      </g>
    </svg>
  );
}

export default WithIcons(ListIcon);
