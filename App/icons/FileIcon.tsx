import React from 'react';
import WithIcons from "./withProps";

function FileIcon(props: any) {
    return (
      <svg {...props}
        xmlns="http://www.w3.org/2000/svg" 
        style={props.style}
        width={props.width}
        height={props.height}
        className={`bi bi-file-ruled ${props.className}`}
        viewBox="0 0 16 16">
        <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v4h10V2a1 1 0 0 0-1-1H4zm9 6H6v2h7V7zm0 3H6v2h7v-2zm0 3H6v2h6a1 1 0 0 0 1-1v-1zm-8 2v-2H3v1a1 1 0 0 0 1 1h1zm-2-3h2v-2H3v2zm0-3h2V7H3v2z"/>
    </svg>
    )
}

export default WithIcons(FileIcon);
