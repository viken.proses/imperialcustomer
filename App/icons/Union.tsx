import React from 'react';
import WithIcons from "./withProps";

const Union = (props: any) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
    className="bi bi-union" 
    viewBox="0 0 16 16">
  <path d="M0 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v2h2a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2v-2H2a2 2 0 0 1-2-2V2z"/>
</svg>
  )
}

export default WithIcons(Union);