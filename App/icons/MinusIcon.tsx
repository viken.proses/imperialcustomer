import React from 'react';
import WithIcons from "./withProps";

function MinusIcon(props: any) {
  return (
    <svg {...props}
      xmlns="http://www.w3.org/2000/svg" 
      style={props.style}
      width={props.width}
      height={props.height}
      className={`bi bi-dash-square text-danger ${props.className}`} 
    viewBox="0 0 16 16">
    <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
  </svg>
  )
}

export default WithIcons(MinusIcon);