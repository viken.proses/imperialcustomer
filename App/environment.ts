const mode = {
    development: "development",
    staging: "staging",
    production: "production"
}
export default mode.development